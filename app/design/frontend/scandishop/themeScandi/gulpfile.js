// /**
//  * @category    Scandishop
//  * @package     Scandishop/themeScandi
//  * @author      Aina Tomsone <info@scandiweb.com>
//  * @copyright   Copyright (c) 2018 Scandiweb, Inc (https://scandiweb.com)
//  * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
//  */
var gulp = require('gulp');
var sass = require('gulp-sass');

sass.compiler = require('node-sass');

gulp.task('sass', function () {
    return gulp.src('./web/css/source/*.scss')

        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./web/css'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./web/css/source/*.scss', ['sass']);
});
