/**
* @category    Scandishop
* @package     Scandishop/themeScandi
* @author      Aina Tomsone <info@scandiweb.com>
* @copyright   Copyright (c) 2018 Scandiweb, Inc (https://scandiweb.com)
* @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
*/
var config = {
    paths: {
        slick:'js/slick'
    },
    shim: {
        slick: {
            deps: ['jquery']
        }
    },
    map:{
        "*":{
            "related-product-slider":'js/related-product-slider'
        }
    }};