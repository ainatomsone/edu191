<?php
/**
 * @category    Scandishop
 * @package     Scandishop/themeScandi
 * @author      Aina Tomsone <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Locator\StoreLocator\Setup;

use Locator\StoreLocator\Api\Data\LocationInterface;
use Locator\StoreLocator\Model\Locations;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * Class InstallData
 * @package Locator\StoreLocator\Setup
 */
class InstallData implements InstallDataInterface
{
    /**
     * Page factory
     *
     * @var Locations
     */
    private $Locations;

    /**
     * @var SerializerInterface
     */
    private $serialize;

    /**
     * Init
     *
     * @param Locations $Locations
     * @param SerializerInterface $serialize
     */
    public function __construct(Locations $Locations, SerializerInterface $serialize)
    {
        $this->Locations = $Locations;
        $this->serialize = $serialize;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $dataArr = [
            [
                LocationInterface::STORE_NAME => 'test_Store_name_1',
                LocationInterface::LON => 36.778259,
                LocationInterface::LAT => 24.097007,
                LocationInterface::ADDRESS => 'test_Address_1',
                LocationInterface::WORKING_HOURS =>
                    $this->serialize->serialize([
                        'Monday' => '08:00 - 18:00',
                        'Tuesday' => '08:00 - 18:00',
                        'Wednesday' => '08:00 - 18:00',
                        'Thursday' => '08:00 - 18:00',
                        'Friday' => '08:00 - 18:00',
                        'Saturday' => '08:00 - 18:00',
                        'Sunday' => '08:00 - 18:00'
                    ])
            ],
            [
                LocationInterface::STORE_NAME => 'test_Store_name_2',
                LocationInterface::ADDRESS => 'test_Address_2',
                LocationInterface::LON => 56.980815,
                LocationInterface::LAT => 24.099679,
                LocationInterface::WORKING_HOURS =>
                    $this->serialize->serialize([
                        'Monday' => '08:00 - 18:00',
                        'Tuesday' => '08:00 - 18:00',
                        'Wednesday' => '08:00 - 18:00',
                        'Thursday' => '08:00 - 18:00',
                        'Friday' => '08:00 - 18:00',
                        'Saturday' => '08:00 - 18:00',
                        'Sunday' => '08:00 - 18:00'
                    ])
            ],
            [
                LocationInterface::STORE_NAME => 'test_Store_name_3',
                LocationInterface::ADDRESS => 'test_Address_3',
                LocationInterface::LON => 56.979017,
                LocationInterface::LAT => 24.126751,
                LocationInterface::WORKING_HOURS =>
                    $this->serialize->serialize([
                        'Monday' => '08:00 - 18:00',
                        'Tuesday' => '08:00 - 18:00',
                        'Wednesday' => '08:00 - 18:00',
                        'Thursday' => '08:00 - 18:00',
                        'Friday' => '08:00 - 18:00',
                        'Saturday' => '08:00 - 18:00',
                        'Sunday' => '08:00 - 18:00'
                    ])
            ],
            [
                LocationInterface::STORE_NAME => 'test_Store_name_4',
                LocationInterface::ADDRESS => 'test_Address_4',
                LocationInterface::LON => 56.963774,
                LocationInterface::LAT => 24.150761,
                LocationInterface::WORKING_HOURS =>
                    $this->serialize->serialize([
                        'Monday' => '08:00 - 18:00',
                        'Tuesday' => '08:00 - 18:00',
                        'Wednesday' => '08:00 - 18:00',
                        'Thursday' => '08:00 - 18:00',
                        'Friday' => '08:00 - 18:00',
                        'Saturday' => '08:00 - 18:00',
                        'Sunday' => '08:00 - 18:00'
                    ])
            ]
        ];
        foreach ($dataArr as $data) {
            $this->Locations->setData($data)->save();
        }
    }
}