<?php
/**
 * @category    Scandishop
 * @package     Scandishop/themeScandi
 * @author      Aina Tomsone <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Locator\StoreLocator\Setup;

use Locator\StoreLocator\Api\Data\LocationInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

/**
 * Class InstallSchema
 * @package Locator\StoreLocator\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        if (version_compare($context->getVersion(), '0.0.2', '<')) {
            if (!$installer->tableExists(LocationInterface::TABLE_NAME)) {
                $table = $installer->getConnection()->newTable(
                    $installer->getTable(LocationInterface::TABLE_NAME)
                )->addColumn(
                    LocationInterface::ID,
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary' => true,
                        'unsigned' => true,
                    ],
                    'ID'
                )->addColumn(
                    LocationInterface::STORE_NAME,
                    Table::TYPE_TEXT,
                    255,
                    ['nullable => false'],
                    'Store name'
                )->addColumn(
                    LocationInterface::LAT,
                    Table::TYPE_DECIMAL,
                     [10,6],
                    ['nullable' => false],
                    'Latitude'
                )->addColumn(
                    LocationInterface::LON,
                    Table::TYPE_DECIMAL,
                    [10,6],
                    ['nullable' => false],
                    'Longitude'
                )->addColumn(
                    LocationInterface::ADDRESS,
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Address'
                )->addColumn(
                    LocationInterface::WORKING_HOURS,
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Working Hours'
                )->addColumn(
                    LocationInterface::CREATED_AT,
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                    'Created At'
                )->setComment('Store Locations');
                $installer->getConnection()->createTable($table);
            }
        }
        $installer->endSetup();
    }
}
