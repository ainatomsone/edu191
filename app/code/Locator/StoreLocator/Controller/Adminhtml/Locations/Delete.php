<?php
/**
 * @category    Scandishop
 * @package     Scandishop/themeScandi
 * @author      Aina Tomsone <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Locator\StoreLocator\Controller\Adminhtml\Locations;

use Locator\StoreLocator\Api\Data\LocationInterface;
use Locator\StoreLocator\Model\Locations;
use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

/**
 * Class Delete
 * @package Locator\StoreLocator\Controller\Adminhtml\Locations
 */
class Delete extends Action
{
    /**
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam(LocationInterface::ID);

        try {
            if ($id) {
                $model = $this->_objectManager->create(Locations::class);
                $model->load($id);

                $model->delete();

                $this->messageManager->addSuccessMessage(__('The location has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            return $resultRedirect->setPath('*/*/add', [LocationInterface::ID => $id]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
