<?php
/**
 * @category    Scandishop
 * @package     Scandishop/themeScandi
 * @author      Aina Tomsone <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Locator\StoreLocator\Controller\Adminhtml\Locations;

use InvalidArgumentExceptio;
use Magento\Backend\App\Action;
use Locator\StoreLocator\Api\Data\LocationInterface;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Serialize\SerializerInterface;
use \Locator\StoreLocator\Model\Locations;
use Symfony\Component\Debug\Exception\FatalErrorException;

/**
 * Class Save
 * @package Locator\StoreLocator\Controller\Adminhtml\Locations
 */
class Save extends Action
{
    /**
     * @var SerializerInterface
     */
    private $serialize;

    /**
     * @var Locations
     */
    private $locations;

    /**
     * @var bool
     */
    private $errors = false;

    /**
     * Save constructor.
     * @param Context $context
     * @param Locations $Locations
     * @param SerializerInterface $serialize
     */
    public function __construct(
        Context $context,
        Locations $Locations,
        SerializerInterface $serialize
    ) {
        $this->serialize = $serialize;
        $this->locations = $Locations;
        parent::__construct($context);
    }

    /**
     * @return mixed
     */
    public function execute()
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $postParams = $this->getRequest()->getParams();
        try {
            $this::isEmptyValidation([$postParams['store_name'], 'Store Name']);
            $this::numberValidation([$postParams['lon'], 'Longitude']);
            $this::numberValidation([$postParams['lat'], 'Latitude']);
            $this::isEmptyValidation([$postParams['address'], 'Address']);
            $this::timeRangeValidation([$postParams['monday'], 'Monday']);
            $this::timeRangeValidation([$postParams['tuesday'], 'Tuesday']);
            $this::timeRangeValidation([$postParams['wednesday'], 'Wednesday']);
            $this::timeRangeValidation([$postParams['thursday'], 'Thursday']);
            $this::timeRangeValidation([$postParams['friday'], 'Friday']);
            $this::timeRangeValidation([$postParams['saturday'], 'Saturday']);
            $this::timeRangeValidation([$postParams['sunday'], 'Sunday']);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }

        if (!$this->errors) {
            if ($postParams) {
                $data = [
                    LocationInterface::STORE_NAME => $postParams['store_name'],
                    LocationInterface::LON => $postParams['lon'],
                    LocationInterface::LAT => $postParams['lat'],
                    LocationInterface::ADDRESS => $postParams['address'],
                    LocationInterface::WORKING_HOURS =>
                        $this->serialize->serialize([
                            LocationInterface::MONDAY => $postParams['monday'],
                            LocationInterface::TUESDAY => $postParams['tuesday'],
                            LocationInterface::WEDNESDAY => $postParams['wednesday'],
                            LocationInterface::THURSDAY => $postParams['thursday'],
                            LocationInterface::FRIDAY => $postParams['friday'],
                            LocationInterface::SATURDAY => $postParams['saturday'],
                            LocationInterface::SUNDAY => $postParams['sunday']
                        ])
                ];

                if ($postParams['id']) {
                    $data[LocationInterface::ID] = $postParams['id'];
                }

                $this->locations->setData($data)->save();
                $this->messageManager->addSuccessMessage('Location data has been saved');
                return $resultRedirect->setPath('*/*/Index');
            }
        } else {
            if ($postParams['id']) {
                $data[LocationInterface::ID] = $postParams['id'];
                $id = $data[LocationInterface::ID];
                return $resultRedirect->setPath('*/*/Add', ['id' => $id]);
            } else {
                return $resultRedirect->setPath("*/*/Add");
            }
        }
    }

    /**
     * @param $object
     */
    private function timeRangeValidation($object)
    {
        $pattern = "/^(?:2[0-3]|[01]\d):[0-5]\d-(?:2[0-3]|[01]\d):[0-5]\d$/";
        if (!preg_match($pattern, $object[0])) {
            $this->messageManager->addErrorMessage(__('%1 has not been set correctly.', $object[1]));
            $this->errors = true;
        }
    }

    /**
     * @param $object
     */
    private function numberValidation($object)
    {
        $pattern = "/^\d+$/";
        if (!preg_match($pattern, $object[0])) {
            $this->messageManager->addErrorMessage(__('%1 has not been set correctly.', $object[1]));
            $this->errors = true;
        }
    }

    /**
     * @param $object
     */
    private function isEmptyValidation($object)
    {
        $pattern = "/[\s\S]/";
        if (!preg_match($pattern, $object[0])) {
            $this->messageManager->addErrorMessage(__('%1 has not been set correctly.', $object[1]));
            $this->errors = true;
        }
    }
}
