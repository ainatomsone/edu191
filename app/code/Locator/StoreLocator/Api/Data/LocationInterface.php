<?php
/**
 * @category    Scandishop
 * @package     Scandishop/themeScandi
 * @author      Aina Tomsone <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Locator\StoreLocator\Api\Data;

/**
 * Interface LocationInterface
 * @package Locator\StoreLocator\Api\Data
 */
interface LocationInterface {
    const TABLE_NAME = 'stores_available';
    const ID = 'id';
    const STORE_NAME = 'store_name';
    const ADDRESS = 'address';
    const LAT = 'lat';
    const LON = 'lon';
    const WORKING_HOURS = 'working_hours';
    const CREATED_AT = 'created_at';
    const MONDAY='Monday';
    const TUESDAY='Tuesday';
    const WEDNESDAY='Wednesday';
    const THURSDAY='Thursday';
    const FRIDAY='Friday';
    const SATURDAY='Saturday';
    const SUNDAY='Sunday';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get STORE_NAME
     *
     * @return string|null
     */
    public function getStoreName();

    /**
     * Get LAT
     *
     * @return int|null
     */
    public function getLat();

    /**
     * Get LON
     *
     * @return int|null
     */
    public function getLon();

    /**
     * Get ADDRESS
     *
     * @return string|null
     */
    public function getAddress();

    /**
     * Get WORKING_HOURS
     *
     * @return string|null
     */
    public function getWorkigHours();

    /**
     * Get CREATED_AT
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set ID
     *
     * @param int $id
     * @return LocationInterface
     */
    public function setId($id);

    /**
     * Set STORE_NAME
     *
     * @param string $store_Name
     * @return LocationInterface
     */
    public function setStoreName($store_Name);

    /**
     * Set LAT
     *
     * @param int $lat
     * @return LocationInterface
     */
    public function setLat($lat);

    /**
     * Set LON
     *
     * @param int $lon
     * @return LocationInterface
     */
    public function setLon($lon);
    /**
     * Set ADDRESS
     *
     * @param int $address
     * @return LocationInterface
     */
    public function setAddress($address);

    /**
     * Set WORKING_HOURS
     *
     * @param int $working_hours
     * @return LocationInterface
     */
    public function setWorkingHours($working_hours);

    /**
     * Set CREATED_AT
     *
     * @param int $created_at
     * @return LocationInterface
     */
    public function setCreatedAt($created_at);
}