<?php
/**
 * @category    Scandishop
 * @package     Scandishop/themeScandi
 * @author      Aina Tomsone <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Locator\StoreLocator\Model\ResourceModel\Locations;

use Locator\StoreLocator\Model\Locations;
use \Locator\StoreLocator\Model\ResourceModel\Locations as ResourceModelLocations;
use Magento\Framework\Controller\ResultFactory;
use \Magento\Cms\Model\ResourceModel\AbstractCollection;
use Magento\Store\Model\Store;

/**
 * Class Collection
 * @package Locator\StoreLocator\Model\ResourceModel\Locations
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * @var string
     */
    protected $_eventPrefix = 'stores_available';

    /**
     * @var string
     */
    protected $_eventObject = 'stores_available_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(Locations::class, ResourceModelLocations::class);
    }

    /**
     * Add filter by store
     *
     * @param int|array|Store $store
     * @param bool $withAdmin
     * @return $this
     */
    public function addStoreFilter($store, $withAdmin = true)
    {
        $this->performAddStoreFilter($store, $withAdmin);
        return $this;
    }
}