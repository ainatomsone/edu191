<?php
/**
 * @category    Scandishop
 * @package     Scandishop/themeScandi
 * @author      Aina Tomsone <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Locator\StoreLocator\Model;

use Locator\StoreLocator\Api\Data\LocationInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Exception;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Locations
 * @package Locator\StoreLocator\Model
 */
class Locations extends AbstractModel implements IdentityInterface, LocationInterface
{
    /**
     *@var string
     */
    const CACHE_TAG = LocationInterface::TABLE_NAME;

    /**
     * @var string
     */
    protected $_cacheTag = LocationInterface::TABLE_NAME;

    /**
     * @var string
     */
    protected $_eventPrefix = LocationInterface::TABLE_NAME;

    /**
     * Initialize Locations model
     *
     * @return void
     *
     */
    protected function _construct()
    {
        $this->_init(ResourceModel\Locations::class);
    }

    /**
     * @return array|string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return array
     */
    public function getDefaultValues()
    {
        $values = [];
        return $values;
    }

    /**
     * Get ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * Get STORE_NAME
     *
     * @return string|null
     */
    public function getStoreName()
    {
        return $this->getData(self::STORE_NAME);
    }

    /**
     * Get LAT
     *
     * @return int|null
     */
    public function getLat()
    {
        return $this->getData(self::LAT);
    }

    /**
     * Get LON
     *
     * @return int|null
     */
    public function getLon()
    {
        return $this->getData(self::LON);
    }

    /**
     * Get ADDRESS
     *
     * @return string|null
     */
    public function getAddress()
    {
        return $this->getData(self::ADDRESS);
    }

    /**
     * Get WORKING_HOURS
     *
     * @return string|null
     */

    public function getWorkigHours()
    {
        return $this->getData(self::WORKING_HOURS);
    }

    /**
     * Get CREATED_AT
     *
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return LocationInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * Set STORE_NAME
     *
     * @param int $store_name
     * @return LocationInterface
     */
    public function setStoreName($store_name)
    {
        return $this->setData(self::STORE_NAME, $store_name);
    }

    /**
     * Set ADDRESS
     *
     * @param int $address
     * @return LocationInterface
     */
    public function setAddress($address)
    {
        return $this->setData(self::ADDRESS, $address);
    }

    /**
     * Set WORKING_HOURS
     *
     * @param int $working_hours
     * @return LocationInterface
     */
    public function setWorkingHours($working_hours)
    {
        return $this->setData(self::WORKING_HOURS, $working_hours);
    }

    /**
     * Set CREATED_AT
     *
     * @param int $created_at
     * @return LocationInterface
     */
    public function setCreatedAt($created_at)
    {
        return $this->setData(self::CREATED_AT, $created_at);
    }

    /**
     * Set LAT
     *
     * @param int $lat
     * @return LocationInterface
     *
     */
    public function setLat($lat)
    {
        return $this->setData(self::LAT, $lat);
    }

    /**
     * Set LON
     *
     * @param int $lon
     * @return LocationInterface
     */
    public function setLon($lon)
    {
        return $this->setData(self::LAT, $lon);
    }
}
