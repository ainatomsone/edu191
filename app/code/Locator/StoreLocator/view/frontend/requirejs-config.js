/**
 * @category    Scandishop
 * @package     Scandishop/themeScandi
 * @author      Aina Tomsone <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
var config = {
    map: {
        "*": {
            "leaflet": "Locator_StoreLocator/js/vendor/leaflet",
            "locationMap": "js/locationMap"
        }
    }
};
