/**
 * @category    Scandishop
 * @package     Scandishop/themeScandi
 * @author      Aina Tomsone <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
const ID = 'id';
const STORE_NAME = 'store_name';
const LAT = 'lat';
const LON = 'lon';
const ADDRESS = 'address';
const WORKING_HOURS = 'working_hours';
const MONDAY = 'Monday';
const TUESDAY = 'Tuesday';
const WEDNESDAY = 'Wednesday';
const THURSDAY = 'Thursday';
const FRIDAY = 'Friday';
const SATURDAY = 'Saturday';
const SUNDAY = 'Sunday';

define(['jquery', 'leaflet', 'jquery/ui'], function ($, L) {
    "use strict";
    $.widget("scandi.locations", {
        options: {
            data: [],
            leafletConfig: {
                title: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                map: '',
                markers: []
            },
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
            lon: 56.980815,
            lat: 24.099679
        },

        _create: function () {
            this._bind();
            this._on($('[id^=id_]'), {
                click: "markerInfoBind"
            })
        },

        _bind: function () {
            var self = this;
            self.initMap();
            self.storeInfo();
        },

        //Bind marker with specific location block
        markerInfoBind: function (event) {
            if (typeof event != 'undefined') {
                let item = event.currentTarget.id;
                let markerId = item.replace(/\D+/, '') - 1;
                $(`#${item}`).toggleClass("selected");
                this.options.leafletConfig.markers[markerId].fire('click');
            }
        },

        //Create map and generate location markers
        initMap: function () {
            let i = 0;
            let data = this.options.data[0];
            let workingHours = data[i][WORKING_HOURS];
            const markers = [];

            var map = L.map('mapid').setView([this.options.lon, this.options.lat], 13);

            L.tileLayer(this.options.leafletConfig.title, {
                attribution: '&copy; <a href = "https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(map);

            for (i = 0; i < this.options.data[0].length; i++) {
                markers[i] = L.marker([data[i][LON], data[i][LAT]], `M_id_${data[i][ID]}`).addTo(map)
                    .bindPopup(`<span class = "disp-block popup-store-title">${data[i][STORE_NAME]}</span>` +
                        `<span class = "disp-block popup-store-address">${data[i][ADDRESS]}</span>` +
                        `<div class = "popup-working-hours">` +
                            `<div class = "popup-working-hours-title">` +
                                `<span class = "popup-title-days">Days</span>` +
                                `<span class = "popup-title-hours"> Hours</span></div>` +
                            `<div class = "popup-working-hours-day">` +
                                `<span class = "popup-wh-day">${MONDAY}</span>` +
                                `<span class = "popup-wh-time">${workingHours[MONDAY]}</span></div>` +
                            `<div class = "popup-working-hours-day">` +
                                `<span class = "popup-wh-day">${TUESDAY}</span>` +
                                `<span class = "popup-wh-time">${workingHours[TUESDAY]}</span></div>` +
                            `<div class = "popup-working-hours-day">` +
                                `<span class = "popup-wh-day">${WEDNESDAY}</span>` +
                                `<span class = "popup-wh-time">${workingHours[WEDNESDAY]}</span></div>` +
                            `<div class = "popup-working-hours-day">` +
                                `<span class = "popup-wh-day">${THURSDAY}</span>` +
                                `<span class = "popup-wh-time">${workingHours[THURSDAY]}</span></div>` +
                            `<div class = "popup-working-hours-day">` +
                                `<span class = "popup-wh-day">${FRIDAY}</span>` +
                                `<span class = "popup-wh-time">${workingHours[FRIDAY]}</span></div>` +
                            `<div class = "popup-working-hours-day">` +
                                `<span class = "popup-wh-day">${SATURDAY}</span>` +
                                `<span class = "popup-wh-time">${workingHours[SATURDAY]}</span></div>` +
                            `<div class = "popup-working-hours-day">` +
                                `<span class = "popup-wh-day">${SUNDAY}</span>` +
                                `<span class = "popup-wh-time">${workingHours[SUNDAY]}</span></div></div>`
                    );
            }
            this.options.leafletConfig.markers = markers;
        },

        //generate Location blocks
        storeInfo: function () {
            let infoBlock = $('#blockLocation');
            let i = 0;
            let data = this.options.data[0];
            let workingHours = data[i][WORKING_HOURS];

            for (i = 0; i < this.options.data[0].length; i++) {
                $(infoBlock).append(`<div id="id_${data[i][ID]}" class = "location">` +
                    `<h3 class = "store-title">${data[i][STORE_NAME]}</h3>` +
                    `<span class = "disp-block store-address">${data[i][ADDRESS]}</span>` +
                    `<div class = "working-hours">` +
                        `<div class = "working-hours-title">` +
                            `<span class = "title-days">Days</span>` +
                            `<span class = "title-hours"> Hours</span></div>` +
                        `<div class = "working-hours-day">` +
                            `<span class = "wh-day">${MONDAY}</span>` +
                            `<span class = "wh-time">${workingHours[MONDAY]}</span></div>` +
                        `<div class = "working-hours-day">` +
                            `<span class = "wh-day">${TUESDAY}</span>` +
                            `<span class = "wh-time">${workingHours[TUESDAY]}</span></div>` +
                        `<div class = "working-hours-day">` +
                            `<span class = "wh-day">${WEDNESDAY}</span>` +
                            `<span class = "wh-time">${workingHours[WEDNESDAY]}</span></div>` +
                        `<div class = "working-hours-day">` +
                            `<span class = "wh-day">${THURSDAY}</span>` +
                            `<span class = "wh-time">${workingHours[THURSDAY]}</span></div>` +
                        `<div class = "working-hours-day">` +
                            `<span class = "wh-day">${FRIDAY}</span>` +
                            `<span class = "wh-time">${workingHours[FRIDAY]}</span></div>` +
                        `<div class = "working-hours-day">` +
                            `<span class = "wh-day">${SATURDAY}</span>` +
                            `<span class = "wh-time">${workingHours[SATURDAY]}</span></div>` +
                        `<div class = "working-hours-day">` +
                            `<span class = "wh-day">${SUNDAY}</span>` +
                            `<span class = "wh-time">${workingHours[SUNDAY]}</span></div></div></div>`);
            }
        },
    });
    return $.scandi.locations;
});
