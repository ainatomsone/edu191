<?php
/**
 * @category    Scandishop
 * @package     Scandishop/themeScandi
 * @author      Aina Tomsone <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Locator\StoreLocator\Block\Locations;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Locator\StoreLocator\Model\ResourceModel\Locations\CollectionFactory;
use Magento\Framework\Serialize\SerializerInterface;

/**
 * Dashboard Customer Info
 *
 * @api
 * @since 100.0.2
 */
class Index extends Template
{
    /** @var \Locator\StoreLocator\Model\ResourceModel\Locations\Collection CollectionFactory */
    protected $collectionFactory;

    /**
     * @var SerializerInterface
     */
    protected $serializeInterface;

    /**
     * Index constructor.
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Context $context,
        CollectionFactory $collectionFactory,
        SerializerInterface $serializeInterface
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->serializeInterface = $serializeInterface;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getAllLocations()
    {
        $rezult = [];
        $items = $this->collectionFactory->create()->getItems();

        foreach ($items as $item) {
            $item->getData('working_hours') ? $workingH = $this->serializeInterface->
            unserialize($item->getData('working_hours')) : $workingH = [];

            $rezult[] = [
                'id' => $item->getId(),
                'store_name' => $item->getData('store_name'),
                'address' => $item->getAddress(),
                'lat' => $item->getLat(),
                'lon' => $item->getLon(),
                'working_hours' => $workingH,
                'created_at' => $item->getCreatedAt()
            ];
        }
        return $this->serializeInterface->serialize($rezult);
    }
}
